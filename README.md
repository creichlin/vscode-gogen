# vscode-gogen

Gogen plugin for vscode

## Features

Minimal syntax highlighter for the gogen template language.
Diagnostics using the gogen tool

## Install

Gogen tool needs to be installed

    go get -u gitlab.com/akabio/gogen/cmd/gogen
    go install gitlab.com/akabio/gogen/cmd/gogen

To build it run

    yarn install
    yarn compile

Install it as vscode extension by copying it into the vscode extensions folder

    cp -r ./* ~/.vscode/extensions/vscode-codegen
