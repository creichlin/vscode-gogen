import * as vscode from "vscode";
import * as child_process from "child_process";

type Err = {
  message: string;
  line: number;
  column: number;
};

export function activate(context: vscode.ExtensionContext) {
  const output = vscode.window.createOutputChannel("GoGen");
  output.appendLine("Extension activated");

  const collection = vscode.languages.createDiagnosticCollection("gg");
  if (vscode.window.activeTextEditor) {
    updateDiagnostics(
      vscode.window.activeTextEditor.document,
      collection,
      output
    );
  }
  context.subscriptions.push(
    vscode.window.onDidChangeActiveTextEditor((editor) => {
      if (editor) {
        updateDiagnostics(editor.document, collection, output);
      }
    })
  );

  context.subscriptions.push(
    vscode.workspace.onDidChangeTextDocument((ce) => {
      updateDiagnostics(ce.document, collection, output);
    })
  );
}

function updateDiagnostics(
  document: vscode.TextDocument,
  collection: vscode.DiagnosticCollection,
  output: vscode.OutputChannel
): void {
  if (document && document.uri.toString().endsWith(".gg")) {
    const ct = document.getText();

    const ggx = child_process.exec(
      "gogen",
      {},
      (err: number, stdout: string, _stderr: string) => {
        const errs: Err[] = JSON.parse(stdout).errors;
        // output.appendLine("return...");
        setErrors(errs);
      }
    );

    ggx.stdin.write(ct);
    ggx.stdin.end();

    function setErrors(errs: Err[] | null) {
      if (errs == null) {
        collection.set(document.uri, undefined);
        return;
      }

      // output.appendLine(`errs ${errs.length}`);
      // output.appendLine(document.uri.toString());
      collection.set(
        document.uri,
        errs.map(
          (e): vscode.Diagnostic => {
            return {
              message: e.message,
              severity: vscode.DiagnosticSeverity.Error,
              range: new vscode.Range(
                new vscode.Position(e.line, e.column),
                new vscode.Position(e.line, e.column + 1)
              ),
            };
          }
        )
      );
    }
  } else {
    collection.clear();
  }
}
