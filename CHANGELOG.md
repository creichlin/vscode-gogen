# Change Log

Notable changes to the "vscode-gogen" extension will be documented in this file.

## [Unreleased]

- Simple syntax highlighting
- Diagnostics using gogen tool
